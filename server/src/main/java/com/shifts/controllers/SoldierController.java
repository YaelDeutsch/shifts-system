package com.shifts.controllers;

import com.shifts.entities.Soldier;
import com.shifts.services.SoldierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
public class SoldierController
{
    @Autowired
    private SoldierService soldierService;

    @GetMapping("/soldiers")
    public List<Soldier> getAllSoldiers()
    {
        return soldierService.getAll();
    }
}
