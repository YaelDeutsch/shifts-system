package com.shifts.controllers;

import com.shifts.contracts.AssignShiftContract;
import com.shifts.entities.Shift;
import com.shifts.entities.Soldier;
import com.shifts.services.ShiftService;
import com.shifts.services.SoldierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
public class ShiftController
{
    @Autowired
    private ShiftService shiftService;
    @Autowired
    private SoldierService soldierService;

    @GetMapping("/shifts")
    public List<Shift> getAllShifts()
    {
        return shiftService.getAll();
    }

    @PostMapping("/saveShifts")
    public void saveShifts(@RequestBody List<Shift> shiftsToSave)
    {
        shiftService.save(shiftsToSave);
    }

    @PostMapping("/assignShift")
    public void assignSoldierToShift(@RequestBody AssignShiftContract assignShiftContract)
    {
        Optional<Shift> shiftToUpdate = shiftService.getById(assignShiftContract.getShiftId());
        Optional<Soldier> assignedSoldier = soldierService.getById(assignShiftContract.getSoldierId());

        if (assignedSoldier.isPresent() && shiftToUpdate.isPresent() &&
                soldierService.isSoldierAssignable(assignedSoldier.get(), shiftToUpdate.get()))
        {
            shiftService.assignSoldier(shiftToUpdate.get(), assignedSoldier.get());
        }
    }
}
