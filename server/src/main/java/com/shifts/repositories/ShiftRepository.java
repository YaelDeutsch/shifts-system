package com.shifts.repositories;

import com.shifts.entities.Shift;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShiftRepository extends JpaRepository<Shift, Long>
{
}
