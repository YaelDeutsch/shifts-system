package com.shifts.repositories;

import com.shifts.entities.Soldier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SoldierRepository extends JpaRepository<Soldier, Long>
{
}
