package com.shifts.contracts;

import lombok.Data;

@Data
public class AssignShiftContract
{
    private Long shiftId;
    private Long soldierId;
}
