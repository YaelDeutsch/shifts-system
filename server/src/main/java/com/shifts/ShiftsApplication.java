package com.shifts;

import com.shifts.entities.Shift;
import com.shifts.entities.ShiftTypeEnum;
import com.shifts.entities.Soldier;
import com.shifts.repositories.ShiftRepository;
import com.shifts.repositories.SoldierRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootApplication
public class ShiftsApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(ShiftsApplication.class, args);
    }

    @Bean
    ApplicationRunner init(ShiftRepository shiftRepository, SoldierRepository soldierRepository)
    {
        return args -> {
            Shift firstShift = new Shift(LocalDateTime.of(2020, 3, 29, 18, 0), LocalDateTime.of(2020, 3, 29, 22, 0), ShiftTypeEnum.ARMORY);
            Shift secondShift = new Shift(LocalDateTime.of(2020, 3, 29, 22, 0), LocalDateTime.of(2020, 3, 30, 2, 0), ShiftTypeEnum.FRONT_GATE);
            shiftRepository.save(firstShift);
            shiftRepository.save(secondShift);
            Soldier firstSoldier = new Soldier((long) 8259660, "יעל דויטש");
            Soldier secondSoldier = new Soldier((long) 1234567, "מתן בכר");
            Soldier thirdSoldier = new Soldier((long) 7654321, "איתי אבירן");
            soldierRepository.saveAll(List.of(firstSoldier, secondSoldier, thirdSoldier));
        };
    }
}
