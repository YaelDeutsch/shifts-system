package com.shifts.services;

import com.shifts.entities.Shift;
import com.shifts.entities.Soldier;
import com.shifts.repositories.ShiftRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShiftService extends BaseService<Shift, Long, ShiftRepository>
{
    public void assignSoldier(Shift shiftToUpdate, Soldier assignedSoldier)
    {
        shiftToUpdate.setSoldier(assignedSoldier);
        this.save(List.of(shiftToUpdate));
    }
}
