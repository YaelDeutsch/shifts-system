package com.shifts.services;

import com.shifts.entities.Shift;
import com.shifts.entities.Soldier;
import com.shifts.repositories.SoldierRepository;
import org.springframework.stereotype.Service;

@Service
public class SoldierService extends BaseService<Soldier, Long, SoldierRepository>
{
    public boolean isSoldierAssignable(Soldier soldier, Shift shiftToAssign)
    {
        return true;
    }
}
