package com.shifts.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public class BaseService<T, ID, Repo extends JpaRepository<T, ID>>
{
    @Autowired
    private Repo repo;

    public List<T> getAll()
    {
        return repo.findAll();
    }

    public Optional<T> getById(ID id)
    {
        return repo.findById(id);
    }

    public void save(List<T> entitiesToSave)
    {
        repo.saveAll(entitiesToSave);
    }
}
