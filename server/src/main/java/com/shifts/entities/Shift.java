package com.shifts.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Shift
{
    @Id
    @GeneratedValue
    private Long id;
    //    @NotNull(message = "יש להזין זמן התחלה")
    private LocalDateTime startTime;
    //    @NotNull(message = "יש להזין זמן סיום")
    private LocalDateTime endTime;
    @NotNull(message = "יש להזין סוג שמירה")
    private ShiftTypeEnum type;
    @ManyToOne(targetEntity = Soldier.class)
    private Soldier soldier;

    public Shift(LocalDateTime startTime, LocalDateTime endTime, ShiftTypeEnum shiftTypeEnum)
    {
        this.startTime = startTime;
        this.endTime = endTime;
        this.type = shiftTypeEnum;
    }
}
