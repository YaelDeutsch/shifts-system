package com.shifts.entities;

public enum ShiftTypeEnum
{
    ARMORY("נשקיה"),
    FRONT_GATE("ש\"ג"),
    OPERATIONS_ROOM("סמב\"צ"),
    PATROL("פטרול"),
    OFFICER("קצין תורן"),
    NON_COMMISSIONED_OFFICER("נגד תורן");

    private String hebValue;

    ShiftTypeEnum(String hebValue)
    {
        this.hebValue = hebValue;
    }
}
