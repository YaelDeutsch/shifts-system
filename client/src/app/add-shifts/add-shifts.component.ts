import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { Group } from '../entities/group';

@Component({
  selector: 'add-shifts',
  templateUrl: './add-shifts.component.html',
  styleUrls: ['./add-shifts.component.css']
})
export class AddShiftsComponent implements OnInit{
  
  assaignControl = new FormControl();
  options: string[] = ['מתן בכר', 'חייל אחר', 'חייל שונה'];
  filteredOptions: Observable<string[]>;
  groupsControl = new FormControl('', Validators.required);
  groups: Group[] = [{ name: 'Dog' }, { name: 'Cat' }, { name: 'Cow' }, { name: 'Fox' }];
  shiftsControl = new FormControl('', Validators.required);
  shifts: string[] = ['רזרבה','לוטם','נכון','עוד'];
  todayDate:Date = new Date()

  constructor() {

  }

  ngOnInit() {
    this.filteredOptions = this.assaignControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): string[] {
    return this.options.filter(option => option.includes(value));
  }
}
