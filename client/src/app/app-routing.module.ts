import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddShiftsComponent } from 'src/app/add-shifts/add-shifts.component';


const routes: Routes = [
  { path: 'addShift', component: AddShiftsComponent }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
